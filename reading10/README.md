Reading 10
==========

1.

a) Print out the error message associated with a failed system call.
execl("system call");
perror("ERROR");

b) Truncate a file.
int ftruncate(file_descriptor, length);

c) Move to the 10th byte of a file.
FILE * fp;
fp = fopen("file.txt", "wb");
fseek(fp, 10, SEEK_SET);
fclose(fp);

d) Check if a path is a directory.
struct stat ps;
stat(PATH, &ps);
bool is_dir = S_ISDIR(ps.st_mode);

e) Create a copy of the current process.
//code
pid_t new_pid = fork();

f) Replace the code in the current process with another program.
pid_t new_pid = fork();
if(childpid == 0){

    exec("NEW PROGRM");
}else{
    //regular code
}

g) Tell a process to terminate.
pid_t new_pid = fork();
kill(new_pid, SIGTERM);

h) Receive the exit status of a child process.
pid_t new_pid = fork();
if (child > 0) {
  int status;
  waitpid(new_pid, &status, 0);

}
