#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>

int main (){
    struct dirent *dire;
    DIR *dir = opendir(".");
    struct stat s;
    if(dir){
        dire =readdir(dir);
        while(dire != NULL){
            if(strcmp(dire->d_name, ".") != 0&& strcmp(dire->d_name, "..") != 0){
                stat(dire->d_name,&s);
                if (S_ISREG(s.st_mode)) printf("%s %lld\n", dire->d_name, (long long int)s.st_size);
            }
            dire =readdir(dir);
        }
        closedir(dir);
    }else{
        puts("Directory couldn't be opened\n");
    }
    return 0;
}
