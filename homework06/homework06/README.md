Homework 06
===========

Activity 1)

1. Describe how your string_reverse_words function works and analyze its time complexity and space complexity in terms of Big-O notation.

This function first reverses the whole string by calling string_reverse_range. After doing this, it goes through the string using a pointer
and determines the starting and ending positions of each word and reverses them by calling string_reverse_range again.

The space complexity in this case is O(1) as the memory used doesen't depend on the input. The time
complexity is also O(n) because the number of times that the program has to flip a word increases linearly with the number of words,
and thus the size of the string.
 												
2. Describe how your string_translate function works and analyze its time complexity and space complexity in terms of Big-O notation.

 This function uses a 2D array that contains the char in the index of the array that concides with its  ASCII decimal number in the first column 
and the char that it is translated to in the second column. At first both columns are the same. The function goes through the string
using a pointer and replaces the char being pointed to with its translation char on the table.

The space complexity is O(1) since the table always has the same size and the memory usedisn't dependent on the input. The time complexity is 
O(n) because the number of times that a char is replaced depends on the length of the string passed.

3. Describe how your string_to_integer function works and analyze its time complexity and space complexity in terms of Big-O notation.

The function first reverses the string passed and then goes through it using a char from front to back. In each iteration the decimal ASCII value
of the char (minus an integer value that depends on whether the char is a digit, upper or lowercase letetr ) is multiplied with a power of
the base that increases in each iteration. The result is added to the integer that is returned.

The space complexity is O(1). The time complexity is O(n) because the number of chars that are processed increases with the length of the string.

4. What is the difference between a shared library such as libstringutils.so and a static library such as libstringutils.a?

	Comparing the sizes of libstringutils.a and libstringutils.so, which one is larger? Why?


Shared libraries are referenced by a a program while it is being executed. Static libraries are added to the program when it is 
compiled (so they become part of it in a way). libstringutils.so is larger than libstringutils.a (14K vs 16K) because .so is its own program
instead of just part of another one.


Activity 2)


2. What is the difference between a static executable such as translate-static and a dynamic executable such as translate-dynamic?

Comparing the sizes of translate-static and translate-dynamic, which one is larger? Why?

Translate static is much bigger than translate dynamic. This is because ell the libraries that are bieng used by translate are included and
compiled with translate-static. On the other hand, translate-dynamic references the libraries used when its running so it takes up less space
in the HD.

When trying to execute translate-static, does it work? If not, explain what you had to do in order for it to actually run.

It works fine.

When trying to execute translate-dynamic, does it work? If not, explain what you had to do in order for it to actually run.

It doesn't work because it needs the shared library containing the string utilities functions and it cannot find the file .so.
