#include "stringutils.h"
#include <ctype.h>
#include <string.h>
#include <math.h>


char *  string_lowercase(char *s) {
    int i=0;
    while(*(s+i)!='\0'){
        *(s+i)=tolower(*(s+i));
        i++;
    }
    return s;
}


char *  string_uppercase(char *s) {
    int i=0;
    while(*(s+i)!='\0'){
        *(s+i)=toupper(*(s+i));
        i++;
    }
    return s;
}

bool    string_startswith(char *s, char *t) {
    int i=0;
    int c=0;
    while(*(s+i)==*(t+i)){
        c++;
        i++;
    }
    if (c!=strlen(t)) return false;
    return true;
}

bool    string_endswith(char *s, char *t) {
    long i=strlen(s)-1;
    long j=strlen(t)-1;
    int c=0;
    while(*(s+i)==*(t+j)){
        c++;
        i--;
        j--;
    }
    if (c!=strlen(t)) return false;
    return true;
}

char *  string_chomp(char *s) {
    char trail=*(s+strlen(s)-1);
    if (trail == '\n'){
        char ns[strlen(s)];
        int i=0;
        while(*(s+i)!='\n'){
            ns[i]=*(s+i);
            i++;
        }
        ns[strlen(s)-1]='\0';
        int ind=0;
        for (char *p = ns; *p; p++){
            *(s+ind)=*p;
            ind++;
        }
        *(s+ind)='\0';
    }
    return s;
}

char *  string_strip(char *s) {
    int i=0;
    long j=strlen(s)-1;
    while( isspace( *(s+i) ) ) i++;
    while( isspace( *(s+j) ) ) j--;
    
    char ns[j-i+2];
    int c=0;
    char a;
    for(int k=i;k<=j;k++){
        a=*(s+k);
        *(ns+c)=a;
        c++;
    }
    *(ns+c)='\0';
    int ind=0;
    for (char *p = ns; *p; p++){
        *(s+ind)=*p;
        ind++;
    }
    *(s+ind)='\0';
    
    return s;
}

static char *   string_reverse_range(char *from, char *to) {
    char *c1=from,*c2=to;
    char a,b;
    long it = ((c2-c1+1)%2) ? (c2-c1)/2 : (c2-c1+1)/2;
    for(int i=0;i<it;i++){
        a=*c1;
        b=*c2;
        *c1=b;
        *c2=a;
        c1++;
        c2--;
    }
    return from;
}

char *  string_reverse(char *s) {
    long len=strlen(s);
    s=string_reverse_range(s, s+len-1);
    return s;
}

char *  string_reverse_words(char *s) {
    
    s=string_reverse_range(s, s+strlen(s)-1);

    int w=0;
    char *c=s, *from=NULL, *to=NULL;
    
    while(*c!='\0'){
        if(!w && !isspace(*c)){
            w=1;
            from=c;
        }
        if(!isspace(*c) && w && (isspace( *(c+1)) || *(c+1)=='\0')){
            to=c;
            string_reverse_range(from, to);
            w=0;
        }
        c++;
    }
    return s;
}

char *  string_translate(char *s, char *from, char *to) {
    char table[256][2];
    for(int i=0;i<256;i++){
        table[i][0]=i;
        table[i][1]=i;
    }
    if(*to!='\0'){
        char *t=to;
        for(char *c=from; *c ; c++){
            table[*c][1]=*t;
            t++;
        }
        
        for(char *c=s; *c ; c++){
            *c=table[*c][1];
        }
    }
    return s;
}

int     string_to_integer(char *s, int base) {
    int result=0, power=0;
    s=string_reverse(s);
    for(char *c=s; *c ;c++){
        if(isdigit(*c)){
            result=(*(c)-48)*pow(base,power) + result;
            power++;
        }else if( *c == 'A' || *c == 'B' || *c == 'C' || *c == 'D' || *c == 'E' || *c == 'F' ){
            result=(*(c)-55)*(int)pow(base,power) + result;
            power++;
        }else if( *c == 'a' || *c == 'b' || *c == 'c' || *c == 'c' || *c == 'c' || *c == 'f' ){
            result=(*(c)-87)*(int)pow(base,power) + result;
            power++;
        }else{
            return 0;
        }
    }
    s=string_reverse(s);
    return result;
}
