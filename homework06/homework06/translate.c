/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {

    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stream)) {
        string_chomp(buffer);
        string_translate(buffer, source, target);
    
        if (mode & 1){
            string_strip(buffer);    
        }
        if (mode & 2){
             string_reverse(buffer);
        }
        if (mode & 4){
             string_reverse_words(buffer);
        }
        if (mode & 8){
             string_lowercase(buffer);
        }
        if (mode & 16){
             string_uppercase(buffer);
        }
        printf("%s\n",buffer);
    }
   
}

enum {
    STRIP = 1<<0,
    REVL = 1<<1,
    REVW = 1<<2,
    LOW = 1<<3,
    UP = 1<<4
};

/* Main Execution */

int main(int argc, char *argv[]) {
    int argind = 1;
    PROGRAM_NAME = argv[0];
    int mode =0;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 's':
                mode |=STRIP;
                break;   
            case 'r':
                mode |=REVL; 
                break;
            case 'w':
                mode |=REVW; 
                break;
            case 'l':
                mode |=LOW; 
                break;
            case 'u':
                mode |=UP; 
                break;
            default:
                usage(1);
                break;
        }
    }
    char *source="";
    char *target="";
    if(argind==argc-2&&argc>1){
        source=argv[argind++];
        target=argv[argind++];
    }
    
    translate_stream(stdin, source, target, mode);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
