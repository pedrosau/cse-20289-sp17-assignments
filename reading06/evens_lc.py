#!/usr/bin/env python2.7

import sys

print ' '.join( [ str(st) for st in [ n for n in [ int(inum) for inum in [ num.strip() for num in sys.stdin ]  ] if not n % 2 ] ] )

