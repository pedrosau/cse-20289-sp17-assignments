#!/usr/bin/env python2.7

import sys

def evens(stream):
    for num in stream:
        num = num.strip()
        if int(num) % 2 == 0:
            yield num

print ' '.join(evens(sys.stdin))
