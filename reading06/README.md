Reading 06
==========

A) MapReduce aims to solve the problems that arise when processing large amounts of data using parallel computing.
When there is a lot of derived data to be processed, multiple computers are required to finish the processing in
a reasonable amount of time. MapReduce aims to take care of the parallelization and errors that may arise during 
this process by hiding the code to do this behind the scenes and letting the programmer focus on the main part of
the project.

B) The first step of the workflow is the creation of a map function. This function takes a key and a value as input.
This function emits an intermediate pair of key and value. This output is then used for the second part of the workflow.
For the second part, the user creates a reduce function which takes the intermediate key and value pair as input.
This function emits a result that comes from the input. The third part of the workflow is the creation of a mapreduce
specification object which contains the filenames for the input and output. All of this comes together when calling the
MapReduce function.
