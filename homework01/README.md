Homework 01
===========

PEDRO SAUNERO

Activity 1)

	a) The two main differences in the ACLs for those folders are the authorized 
	users and administrators.
	b) The system administrator ACL since Private includes the user as a sys 
	admin.
	
Activity 2)

| Command                             | Elapsed Time    |
|-------------------------------------|-----------------|
| cp ...                              | 0.04 seconds    |
| mv ...                              | 0.001 seconds   |
| mv ...                              | 0.19 seconds    |
| rm ...                              | 0.002 seconds   |


	1. The first one only changes the name of the file so basically just rewrites 
	the text while the second one acceses another directory (and part of
	the memory) and actually writes all the files there.
	
	2. In the las operation the computer just deletes the directory instead of 
	writing data.
	
	

Activity 3)

	1. To process math.txt you only need to execute 'bc math.txt'. No IO 
	redirection is required.
	2. For this you would need to use bc math.txt > rsults.txt.
	3. To discard any errors you would need to use 'bc math.txt > results.txt 2> 
	/dev/null'
	4. You would need to use 'cat math.txt | bc'. This is less efficient because 
	you are unnecessarily adding an operation to the equation by first using cat 
	with math.txt and then redirecting the output to bc's input.
	
Activity 4)


	1. 49 accounts have /sbin/nologin as their shell. For this I used 'grep /sbin/nologin passwd | wc'
	2. 57 users are logged in. For this I used 'cat passwd | wc'
	3. The 5 largest files are as follows:
	
		36860	/etcdu 
		8556	/etc/gconf
		7776	/etc/selinux
		7756	/etc/selinux/targeted
		7128	/etc/selinux/targeted/policy
		
		I used 'du /etc | sort -g -r | head -n 5'
	4. 3 instances of bash are running. I used 'grep bash passwd | wc'
	
Activity 5)
	
		1.  a) I tried using ctrl-c (which interupts the program) but it just made the program display another troll.
			b) ctr-z worked and suspended the process.
			
		2.  a) We can use 'ps ux | grep TROLL | awk '{print $2}' | xargs kill -9'. What this does is take the output from
			ps ux and select the lines containing TROLL. The only the second part od those lines (the process ID) is redirected
			as an argument to kill-9. 
			b) kill -9 PID should do the trick but for this we first need the PID.  
		
		3. kill -15 PID, which is the TERMINATE signal, resulted in the program displaying 'nice try noob'.
		kill -1 PID or HANGUP resulted in " Sorry I'm not home right now / I'm \
		\ walking into spiderwebs  " being displayed.
