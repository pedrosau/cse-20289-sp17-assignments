Comments:

- no answers for activity 1 part 2. -1

- activity 2: you are supposed to use io redirection `bc < math.txt`

- 4.2: `who | cut -d ' ' -f 1 | sort | uniq | wc -l` -0.25

- 4.4: `ps aux | grep bash | grep -v grep | wc -l` -0.25

- 5.1.b: Ctrl-Z just suspends the process. it does not terminate it. -0.25

- 5.2.b: `killall -KILL TROLL` would do it in one command

Score: 13.25/15
