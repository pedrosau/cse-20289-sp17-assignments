Reading 09
==========


1. a) The size of s is 4 bytes(char pointer) + 4 bytes (int)= 8 bytes. The size of u 4 bytes since number and string are actually
stored in the same part of the memory.

b) A struct is a collection of variables that are grouped under one data structure. A union is a way of allocating a section 
of the memory that can be accessed by multiple variables with possibly different data types, in other words, multiple variables
share the same memory.

2. a)  fate is inexorable!

b) 

c) Types just determine how many bits are going to be used to store a variable. Everything from integers to chars to structs are
just stored in binary. This is why it is possible to typecast in C. An int containing the value 65  can be typecasted to be read as a char
instead and represent the letter 'A'. When we cats values we just tell the computer to change the number of bits read. For example,
typecasting an int 1 ( 0000 0000 0000 0001) to double (0000 0000 0000 0000 0000 0000 0000 0001).

3. a) A collision happens when when a value is hashed to get the position in a hash table where it would be stored but there is already 
a value in that same position.

b) In a hash table that uses separate chaining, collisions are handled by storing a pointer in the entry for a key. This pointer points
to a data structure holding the information that we want for the key. This data structure, however, contains a pointer to another DS containing
the info that we want for another key that had the same entry in the table. So one entry can point to multiple, separate, structures containing
the desired info.

c) In a hash table that uses open addressing, once a collision has been detected, the program keeps looking on the next entry in the table 
until it finds a spot that doesn't conatin a value yet.

4. a)
| Bucket | Value                       |
|--------|-----------------------------|
| 0      |                             |
| 1      |                             |
| 2      | 2 -> 72                     |
| 3      | 3->13                       |
| 4      | 14                          |
| 5      |                             |
| 6      | 56                          |
| 7      | 7                           |
| 8      | 78->68                      |
| 9      | 79                          |



b)

| Bucket | Value                       |
|--------|-----------------------------|
| 0      |                             |
| 1      |                             |
| 2      | 2                           |
| 3      | 3                           |
| 4      | 72                          |
| 5      | 13                          |
| 6      | 56                          |
| 7      | 7                           |
| 8      | 78                          |
| 9      | 79                          |
| 10     | 68                          |
| 11     | 14                          |
| 12     |                             |
| 13     |                             |
| 14     |                             |
| 15     |                             |
| 16     |                             |
| 17     |                             |
| 18     |                             |
