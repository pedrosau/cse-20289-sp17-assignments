GRADING - Exam 02
=================

- Identification:   2.5
- Web Scraping:     3.25
- Generators:       6
- Concurrency:      4.5
- Translation:      6
- Total:	    22.25
