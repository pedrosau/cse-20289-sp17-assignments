#!/usr/bin/env python2.7

import sys

counts = {}
lnum=set()
for line in sys.stdin:
    line=line.strip()
    k, v  = line.split('\t', 1)

    if k in counts:
        counts[k].add(v)
    else:
        counts[k]=set()
        counts[k].add(v)

for k, v in sorted(counts.items()):
    v=list(v)
    v = [int(x) for x in v]
    v.sort()
    v = [str(x) for x in v]
    print '{}\t{}'.format(k, ' '.join(v) )
