#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
    print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

def md5sum(s):
    
    out = hashlib.md5()
    out.update(str(s))
    return out.hexdigest()

def permutations(length, alphabet=ALPHABET):
    
    if int(length)== 1:
        for l in alphabet:
            yield l
    else:
        for i in range(len(alphabet)):
            letter=alphabet[i]
            for p in permutations(int(length)-1, alphabet):
                nstr=letter+p
                yield nstr

def smash(hashes, length, alphabet=ALPHABET, prefix=''):
    strn=( st for st in (  prefix + per  for per in permutations(length,alphabet) ) )
    res = [ s for s in strn if md5sum(s) in hashes ]
    return res 

# Main Execution

if __name__ == '__main__':
    
    while len(ARGUMENTS) and ARGUMENTS[0].startswith('-') and len(ARGUMENTS[0]) > 1:
        arg = ARGUMENTS.pop(0)
        if arg == '-a':
            ALPHABET = ARGUMENTS.pop(0)
        elif arg == '-c':
            CORES = ARGUMENTS.pop(0)
        elif arg == '-l':
            LENGTH = ARGUMENTS.pop(0)
        elif arg == '-p':
            PREFIX = ARGUMENTS.pop(0)
        elif arg == '-s':
            HASHES = ARGUMENTS.pop(0)
        else:
            usage(1)

    hset=set(line.strip() for line in open(HASHES))

    
    if int(CORES) > 1 and int(LENGTH) > 1:
        pool = multiprocessing.Pool(int(CORES))
        if len(PREFIX) == 0:
            if LENGTH  < 4:
                subsmash = functools.partial(smash, hset, int(LENGTH)-1 , ALPHABET)
                perms=list(permutations(1, ALPHABET))
                result = itertools.chain.from_iterable(pool.imap(subsmash,perms))
            else:
                subsmash = functools.partial(smash, hset, int(LENGTH)-2 , ALPHABET)
                perms=list(permutations(2, ALPHABET))
                result = itertools.chain.from_iterable(pool.imap(subsmash,perms))
        else:
            TLEN = len(PREFIX) + int(LENGTH)
            if TLEN  <= 4:
                subsmash = functools.partial(smash, hset, TLEN-len(PREFIX)-1 , ALPHABET)
                newpref=(PREFIX + per for per in permutations(1, ALPHABET))  
                result = itertools.chain.from_iterable(pool.imap(subsmash,newpref) )
            else:
                subsmash = functools.partial(smash, hset, TLEN-len(PREFIX)-2 , ALPHABET)
                newpref=(PREFIX + per for per in permutations(2, ALPHABET))
                result = itertools.chain.from_iterable(pool.imap(subsmash, newpref ))

    else:
        result= smash(hset, LENGTH, ALPHABET, PREFIX) 
        
    for r in result:
        print r
        

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
