#!/usr/bin/env python2.7

import sys
import string
punct= string.punctuation
punct.replace('-','')
alpha='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-'

count = 0
for line in sys.stdin:
    count=count+1
    for word in line.strip().split():
        word = ''.join(char for char in word if char in alpha)
        print '{}\t{}'.format(word.lower(), count)

