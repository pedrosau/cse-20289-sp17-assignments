Reading 01
==========



1.

a) | is a pipeline. It is used to redirect the output of /dev/null to the
input of sort -h 

b)This redirects the standard error of du -h /etc/ to /dev/null or in other words, that error
is ignored.

c) Indicates that the output of sort -h is going to be written in output.txt

d) The -h flags indicate the output to be human-readable.

e) It's not. Because of the order, the error would be written in the file output.txt.

2.

a) Using: cat 2002*

b) Using cat *12

c) Using: cat  *0[1-6] | less

d)cat  {2002,2004,2006}-{01,03,05,07,09,11} | less

e) cat  {2002..2004}-{09-12} | less

3)

a)The second and third files

b) The second and third files.

c) None of the files

d) chmod 750 Tux

e) chmod 600 Tux


4)

a) I would use kill -19 [Process ID]

b) I would use kill -18 [Process ID]

c) Pressing ctrl-D

d)I would use kill -15 [Process ID]

e)I would use kill -9 [Process ID]
