Reading 08 - Grading
========================
**Score**: 2.75/4

**Grader**: Mimi Chen


Deductions
------------------
(-0.25) Q1.3 

		13 bytes = pointer is 8 bytes + 5 chars (need to include NUL)

(-0.25) Q1.5

		Pointer is 8 bytes

(-0.25) Q1.6 

		24 bytes = Pointer is 8 bytes, and one point struct is 16 byte

(-0.25) Q1.7

		168 bytes = Pointer is 8 bytes, and 10 point structs * 16 bytes per struct

(-0.25) Q1.8

		88 bytes = Pointer is 8 bytes, and 10 pointers to structs * 8 bytes per pointer




Comments
------------------
Pointers require allocation of memory!
