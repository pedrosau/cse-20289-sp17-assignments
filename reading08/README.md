Reading 08
==========


Activity 1)

1. 4 bytes are allocated for an int in a 64 bit machine.
2. 5 ints are allocated so a total of 20 bytes.
3. The string consists of 4 chars plus 1 null char. Each char takes 1 byte so the total is 5 bytes.
4. The struct consists of two doubles so 16 bytes are allocated.
5. No memory is allocated.
6. 16 bytes are allocated.
7. 160 bytes are allocated.
8. No memory is allocated.


Activity 2)

1. The error in task 3 was that the program was allocating far too liitle memory. Malloc receives an argument that represents
the number of bytes to be allocated and the program was originally passing the number of integers to be stored. The solution 
was to simply multiply the number of ints times the size of an int in bytes.

2. When randoms[i] = rand() was changed to randoms[i] = rand() % 1000, the chances of a duplicate appearing in the array increased.
The memory leak came from the fact that whenever a duplicate was detected and duplicates returned true, the memory alocated for the array 
wasn't being released and so it built up. The solution was to simply include free(randoms) before returning true to make sure that all 
the allocated memory was freed.




