#!/usr/bin/env python2.7
import os
import commands



command='shuf -i1-{}  -n {} | ./measure ./freq -l {} > /dev/null'
o11=commands.getoutput(command.format(1,1,0.5)).split()
o12=commands.getoutput(command.format(1,1,0.75)).split()
o13=commands.getoutput(command.format(1,1,0.9)).split()
o14=commands.getoutput(command.format(1,1,1.0)).split()
o15=commands.getoutput(command.format(1,1,2.0)).split()
o16=commands.getoutput(command.format(1,1,4.0)).split()
o17=commands.getoutput(command.format(1,1,8.0)).split()
o18=commands.getoutput(command.format(1,1,16.0)).split()

o21=commands.getoutput(command.format(10,10,0.5)).split()
o22=commands.getoutput(command.format(10,10,0.75)).split()
o23=commands.getoutput(command.format(10,10,0.9)).split()
o24=commands.getoutput(command.format(10,10,1.0)).split()
o25=commands.getoutput(command.format(10,10,2.0)).split()
o26=commands.getoutput(command.format(10,10,4.0)).split()
o27=commands.getoutput(command.format(10,10,8.0)).split()
o28=commands.getoutput(command.format(10,10,16.0)).split()


o31=commands.getoutput(command.format(100,100,0.5)).split()
o32=commands.getoutput(command.format(100,100,0.75)).split()
o33=commands.getoutput(command.format(100,100,0.9)).split()
o34=commands.getoutput(command.format(100,100,1.0)).split()
o35=commands.getoutput(command.format(100,100,2.0)).split()
o36=commands.getoutput(command.format(100,100,4.0)).split()
o37=commands.getoutput(command.format(100,100,8.0)).split()
o38=commands.getoutput(command.format(100,100,16.0)).split()


o41=commands.getoutput(command.format(1000,1000,0.5)).split()
o42=commands.getoutput(command.format(1000,1000,0.75)).split()
o43=commands.getoutput(command.format(1000,1000,0.9)).split()
o44=commands.getoutput(command.format(1000,1000,1.0)).split()
o45=commands.getoutput(command.format(1000,1000,2.0)).split()
o46=commands.getoutput(command.format(1000,1000,4.0)).split()
o47=commands.getoutput(command.format(1000,1000,8.0)).split()
o48=commands.getoutput(command.format(1000,1000,16.0)).split()


o51=commands.getoutput(command.format(10000,10000,0.5)).split()
o52=commands.getoutput(command.format(10000,10000,0.75)).split()
o53=commands.getoutput(command.format(10000,10000,0.9)).split()
o54=commands.getoutput(command.format(10000,10000,1.0)).split()
o55=commands.getoutput(command.format(10000,10000,2.0)).split()
o56=commands.getoutput(command.format(10000,10000,4.0)).split()
o57=commands.getoutput(command.format(10000,10000,8.0)).split()
o58=commands.getoutput(command.format(10000,10000,16.0)).split()


o61=commands.getoutput(command.format(100000,100000,0.5)).split()
o62=commands.getoutput(command.format(100000,100000,0.75)).split()
o63=commands.getoutput(command.format(100000,100000,0.9)).split()
o64=commands.getoutput(command.format(100000,100000,1.0)).split()
o65=commands.getoutput(command.format(100000,100000,2.0)).split()
o66=commands.getoutput(command.format(100000,100000,4.0)).split()
o67=commands.getoutput(command.format(100000,100000,8.0)).split()
o68=commands.getoutput(command.format(100000,100000,16.0)).split()


o71=commands.getoutput(command.format(1000000,1000000,0.5)).split()
o72=commands.getoutput(command.format(1000000,1000000,0.75)).split()
o73=commands.getoutput(command.format(1000000,1000000,0.9)).split()
o74=commands.getoutput(command.format(1000000,1000000,1.0)).split()
o75=commands.getoutput(command.format(1000000,1000000,2.0)).split()
o76=commands.getoutput(command.format(1000000,1000000,4.0)).split()
o77=commands.getoutput(command.format(1000000,1000000,8.0)).split()
o78=commands.getoutput(command.format(1000000,1000000,16.0)).split()


o81=commands.getoutput(command.format(10000000,10000000,0.5)).split()
o82=commands.getoutput(command.format(10000000,10000000,0.75)).split()
o83=commands.getoutput(command.format(10000000,10000000,0.9)).split()
o84=commands.getoutput(command.format(10000000,10000000,1.0)).split()
o85=commands.getoutput(command.format(10000000,10000000,2.0)).split()
o86=commands.getoutput(command.format(10000000,10000000,4.0)).split()
o87=commands.getoutput(command.format(10000000,10000000,8.0)).split()
o88=commands.getoutput(command.format(10000000,10000000,16.0)).split()


print(
'''
| NITEMS   | ALPHA    | TIME      | SPACE     |
| -------- | -------- |-----------|-----------|
| 1        | 0.5      | {}\t| {}\t|
| 1        | 0.75     | {}\t| {}\t|
| 1        | 0.9      | {}\t| {}\t|
| 1        | 1.0      | {}\t| {}\t|
| 1        | 2.0      | {}\t| {}\t|
| 1        | 4.0      | {}\t| {}\t|
| 1        | 8.0      | {}\t| {}\t|
| 1        | 16.0     | {}\t| {}\t|
| 10       | 0.5      | {}\t| {}\t|
| 10       | 0.75     | {}\t| {}\t|
| 10       | 0.9      | {}\t| {}\t|
| 10       | 1.0      | {}\t| {}\t|
| 10       | 2.0      | {}\t| {}\t|
| 10       | 4.0      | {}\t| {}\t|
| 10       | 8.0      | {}\t| {}\t|
| 10       | 16.0     | {}\t| {}\t|
| 100      | 0.5      | {}\t| {}\t|
| 100      | 0.75     | {}\t| {}\t|
| 100      | 0.9      | {}\t| {}\t|
| 100      | 1.0      | {}\t| {}\t|
| 100      | 2.0      | {}\t| {}\t|
| 100      | 4.0      | {}\t| {}\t|
| 100      | 8.0      | {}\t| {}\t|
| 100      | 16.0     | {}\t| {}\t|
| 1000     | 0.5      | {}\t| {}\t|
| 1000     | 0.75     | {}\t| {}\t|
| 1000     | 0.9      | {}\t| {}\t|
| 1000     | 1.0      | {}\t| {}\t|
| 1000     | 2.0      | {}\t| {}\t|
| 1000     | 4.0      | {}\t| {}\t|
| 1000     | 8.0      | {}\t| {}\t|
| 1000     | 16.0     | {}\t| {}\t|
| 10000    | 0.5      | {}\t| {}\t|
| 10000    | 0.75     | {}\t| {}\t|
| 10000    | 0.9      | {}\t| {}\t|
| 10000    | 1.0      | {}\t| {}\t|
| 10000    | 2.0      | {}\t| {}\t|
| 10000    | 4.0      | {}\t| {}\t|
| 10000    | 8.0      | {}\t| {}\t|
| 10000    | 16.0     | {}\t| {}\t|
| 100000   | 0.5      | {}\t| {}\t|
| 100000   | 0.75     | {}\t| {}\t|
| 100000   | 0.9      | {}\t| {}\t|
| 100000   | 1.0      | {}\t| {}\t|
| 100000   | 2.0      | {}\t| {}\t|
| 100000   | 4.0      | {}\t| {}\t|
| 100000   | 8.0      | {}\t| {}\t|
| 100000   | 16.0     | {}\t| {}\t|
| 1000000  | 0.5      | {}\t| {}\t|
| 1000000  | 0.75     | {}\t| {}\t|
| 1000000  | 0.9      | {}\t| {}\t|
| 1000000  | 1.0      | {}\t| {}\t|
| 1000000  | 2.0      | {}\t| {}\t|
| 1000000  | 4.0      | {}\t| {}\t|
| 1000000  | 8.0      | {}\t| {}\t|
| 1000000  | 16.0     | {}\t| {}\t|
| 10000000 | 0.5      | {}\t| {}\t|
| 10000000 | 0.75     | {}\t| {}\t|
| 10000000 | 0.9      | {}\t| {}\t|
| 10000000 | 1.0      | {}\t| {}\t|
| 10000000 | 2.0      | {}\t| {}\t|
| 10000000 | 4.0      | {}\t| {}\t|
| 10000000 | 8.0      | {}\t| {}\t|
| 10000000 | 16.0     | {}\t| {}\t|
'''.format(\
o11[0],o11[2],o12[0],o12[2],o13[0],o13[2],o14[0],o14[2],o15[0],o15[2],o16[0],o16[2],o17[0],o17[2],o18[0],o18[2], \
o21[0],o21[2],o22[0],o22[2],o23[0],o23[2],o24[0],o24[2],o25[0],o25[2],o26[0],o26[2],o27[0],o27[2],o28[0],o28[2], \
o31[0],o31[2],o32[0],o32[2],o33[0],o33[2],o34[0],o34[2],o35[0],o35[2],o36[0],o36[2],o37[0],o37[2],o38[0],o38[2],\
o41[0],o41[2],o42[0],o42[2],o43[0],o43[2],o44[0],o44[2],o45[0],o45[2],o46[0],o46[2],o47[0],o47[2],o48[0],o48[2],\
o51[0],o51[2],o52[0],o52[2],o53[0],o53[2],o54[0],o54[2],o55[0],o55[2],o56[0],o56[2],o57[0],o57[2],o58[0],o58[2],\
o61[0],o61[2],o62[0],o62[2],o63[0],o63[2],o64[0],o64[2],o65[0],o65[2],o66[0],o66[2],o67[0],o67[2],o68[0],o68[2],\
o71[0],o71[2],o72[0],o72[2],o73[0],o73[2],o74[0],o74[2],o75[0],o75[2],o76[0],o76[2],o77[0],o77[2],o78[0],o78[2],\
o71[0],o81[2],o82[0],o82[2],o83[0],o83[2],o84[0],o84[2],o85[0],o85[2],o86[0],o86[2],o87[0],o87[2],o88[0],o88[2] \
) )
