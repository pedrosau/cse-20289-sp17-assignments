/* entry.c: map entry */

#include "map.h"


Entry *		entry_create(const char *key, const Value value, Entry *next, Type type) {
  Entry *en = calloc(1,sizeof(Entry));
  if(en!=NULL){
    en->key=strdup(key);
    if(type==STRING) en->value.string=strdup(value.string);
    else en->value.number = value.number;
    en->type=type;
    en->next=next;
    return en;
  }
  return NULL;
}

Entry *		entry_delete(Entry *e, bool recursive) {

  if(recursive){
    Entry *dummy=e;
    while(dummy!=NULL){
        dummy=dummy->next;
        free(e->key);
        if(e->type==STRING) free(e->value.string);
        free(e);
        e=dummy;
    }
  }else{
    free(e->key);
    if(e->type==STRING) free(e->value.string);
    free(e);
  }
  return NULL;
}

void            entry_update(Entry *e, const Value value, Type type) {
    if(e->type==STRING) free(e->value.string);
    e->type = type;
    if(type==STRING) e->value.string=strdup(value.string);
    else e->value.number = value.number;
}

void            entry_dump(Entry *e, FILE *stream, const DumpMode mode) {
    switch(mode){
        case KEY:
            fprintf(stream, "%s\n",e->key);
            break;
        case KEY_VALUE:
            if (e->type==NUMBER) fprintf(stream,"%s\t%ld\n",(char *)e->key,e->value.number);
            else fprintf(stream, "%s\t%s\n",(char *)e->key,e->value.string);
            break;
        case VALUE:
            if (e->type==NUMBER) fprintf(stream,"%ld\n",e->value.number);
            else fprintf(stream, "%s\n",e->value.string);
            break;
        case VALUE_KEY:
            if (e->type==NUMBER) fprintf(stream, "%ld\t%s\n",e->value.number,e->key);
            else fprintf(stream, "%s\t%s\n",e->value.string,e->key);
            break;
    }
}





/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
