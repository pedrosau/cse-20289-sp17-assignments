/* map.c: separate chaining hash table */

#include "map.h"

Map *	        map_create(size_t capacity, double load_factor) {
    Map *m = (Map *)calloc(1,sizeof(Map));
    m->capacity = (capacity==0) ? DEFAULT_CAPACITY : capacity;
    m->load_factor = (load_factor == 0) ? DEFAULT_LOAD_FACTOR : load_factor;
    m->size=0;
    m->buckets = (Entry *)calloc( (m->capacity) , sizeof(Entry) );
    return m;
}


Map *	        map_delete(Map *m) {
    if (m->size>0){

        for(size_t i=0 ; i<m->capacity ; i++ ){
            if(m->buckets[i].next!=NULL)
                entry_delete((m->buckets[i].next), true);
        }
    }
    free(m->buckets);
    free(m);
    return NULL;
}

void            map_insert(Map *m, const char *key, const Value value, Type type) {

    double lofa=(double)(m->size)/(double)(m->capacity);
    if(lofa > m->load_factor) map_resize(m, 2*m->capacity);
    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    Entry *e = map_search(m, key);
    if (e==NULL){
        Entry *nxt =(m->buckets)[bucket].next;
        Entry *new= entry_create(key, value, nxt, type);
        (m->buckets)[bucket].next=new;
        m->size=m->size+1;
    }else{
        entry_update(e, value, type);
    }


}

Entry *         map_search(Map *m, const char *key) {
    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    Entry *e = (m->buckets)[bucket].next;
    if (e==NULL) return NULL;
    while(strcmp((*e).key,key)!=0){
        e = e->next;
        if (e==NULL) return NULL;
    }
    return e;
}

bool           map_remove(Map *m, const char *key) {
    uint64_t bucket = fnv_hash(key, strlen(key)) % m->capacity;
    Entry *prev=(Entry *)&(m->buckets[bucket]);
    Entry *e = (m->buckets[bucket]).next;
    if (e==NULL) return false;
    while(strcmp((*e).key,key)!=0){
        prev=e;
        e = e->next;
        if (e==NULL) return false;
    }
    prev->next=e->next;
    entry_delete(e, false);
    m->size=m->size-1;
    return true;
}

void		map_dump(Map *m, FILE *stream, const DumpMode mode) {

    for(size_t i=0;i<m->capacity;i++){
        Entry *e=m->buckets[i].next;
        while(e!=NULL){
            entry_dump(e,stream, mode);
            e=e->next;
        }
    }

}

void            map_resize(Map *m, size_t new_capacity) {
    Entry *old_buckets = m->buckets;
    size_t old_capacity = m->capacity;
    Entry *new_buckets = (Entry *)calloc(new_capacity, sizeof(Entry));

    for(size_t i=0; i<old_capacity;i++){
        Entry *e=old_buckets[i].next;
        while(e!=NULL){
            Entry *nxt=e->next;

            uint64_t nbucket = fnv_hash(e->key, strlen(e->key)) % new_capacity;
            e->next=new_buckets[nbucket].next;
            new_buckets[nbucket].next=e;
            e=nxt;

        }
    }
    m->buckets=new_buckets;
    m->capacity=new_capacity;
    free(old_buckets);
}






















/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
