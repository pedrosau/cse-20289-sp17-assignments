/* freq.c */

#include "map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "    -f FORMAT        Output format (KEY, KEY_VALUE, VALUE, VALUE_KEY)\n");
    fprintf(stderr, "    -l LOAD_FACTOR   Load factor for hash table\n");
    exit(status);
}

void freq_stream(FILE *stream, double load_factor, DumpMode mode) {
    Map *m = map_create(DEFAULT_CAPACITY, load_factor);

    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stdin)){

        char *line, *key;
        int len = strlen(buffer)-1;
        if (buffer[len] == '\n')  buffer[len] = '\0';
        line=buffer;
        key = strtok (line," ");
        while (key != NULL){

            Entry *e =  map_search(m,key);
            if (e!=NULL){
                Value value =(Value)(e->value.number + 1);
                map_insert(m, key, value, NUMBER);
            }else{
                Value value=(Value)(int64_t)1;
                map_insert(m, key, value, NUMBER);
            }

            key = strtok (NULL, " ");
        }


    }
    map_dump(m, stream, mode);
    map_delete(m);

}

/* Main Execution */

int main(int argc, char *argv[]) {
    double LOAD_FACTOR = 0;
    DumpMode FORMAT = VALUE_KEY;

    int argind = 1;
    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        char *f;
        char *lf;
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 'f':

                f = argv[argind++];
                if( strcmp(f, "KEY")==0 ) FORMAT = KEY;
                else if ( strcmp(f, "VALUE") == 0 ) FORMAT = VALUE;
                else if ( strcmp(f, "KEY_VALUE") == 0 ) FORMAT = KEY_VALUE;
                else if ( strcmp(f, "VALUE_KEY") == 0 ) FORMAT = VALUE_KEY;
                break;
            case 'l':
                //char *lf
                lf = argv[argind++];
                double loadf=atof(lf);
                LOAD_FACTOR=loadf;
                break;
            default:
                usage(1);
                break;
        }
    }

    /* Compute frequencies of data from stdin */
    freq_stream(stdout, LOAD_FACTOR, FORMAT);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
