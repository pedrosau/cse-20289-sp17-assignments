Homework 08
===========

Activity 1)

1. Briefly describe what memory you allocated in entry_create and what memory you deallocated in entry_delete.
How did you handle the recursive flag?

In entry_create I allocated enough memory to hold an Entry struct by using calloc. I also allocated enough memory to hold the key string by using strdup. In entry_delete I first free'd the key, then the value if it was a string and finally the entry itself. If recursive was true I just used a dummy Entry pointer that kept track of the next entry to be deleted until it reached null.

2. Briefly describe what memory you allocated in map_create and what memory you deallocated in map_create.
How did you handle internal entries?

In map_create I allocated enough memory to hold a map struct and enough memory to hold a Entry array with a size equal to the map's capacity. In map_delete I first went through the buckets and recursively deallocated each entry that came after the bucket. Then I free'd the array of buckets and finally the map itself.

3. Briefly describe what memory you allocated, copied, and deallocated in map_resize.
When is this function called and what exactly does it do to grow the map?

In map_resize I allocated enough memory to hold an array of entries with twice the size of the current buckets array. Then I went through the old buckets, hashed and inserted each entry in the new buckets. In the cases where there was more than one entry in a bucket I had to keep track of the next entry that comes after the one being moved to the new bucket in order to go through all of the entries. After moving all entries I free'd the old buckets array.

4. Briefly describe how your map_insert function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

First, map_insert checks if size/capacity has exceeded the load factor. If it has then the table is resized with double the current capacity. Then map_search is called to see if the key already exists in the map. If it does then its value is updated, if it doesn't the key is hashed and then inserted as the next entry on the current bucket.  The time complexity is O(alpha), alpha being size/capacity. We can control alpha by resizing the map once alpha reaches a certain value, so basically the complexity is O(1). The space complexity is also O(1) because the number of variables created always stays the same.



5. Briefly describe how your map_search function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

First, map_search hashes the key and goes to the bucket pointed by the hash value. Then it goes through the entries in the list until it finds one with a matching key. If the key is found then the pointer to the entry is returned, if NULL is reached, the function returns NULL. The time and space complexity are both O(1) for the same reasons stated above.


6. Briefly describe how your map_remove function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).

map_remove works by using two pointers. One is used to find the entry to be deleted and the other one is used to keep track of the previous entry. Once the entry to be deleted is found, the previous entry is linked with the next entry and the current entry is free'd. If the entry isn't found then the function returns NULL.



Activity 2)

1. Based on your experiments, what is the overall trend in terms of time and space as the number of items increases for the different load factors? Are the results surprising to you? Explain why or why not.

Both time and space increase as the number of entries increases, which is not surprising. What's interesting is that as alpha increases, the time increases but the space used decreases. This is not surprising because with a higher alpha, there are going to be more collisions which increases the time it takes to search, insert, remove, etc. However as alpha goes up the map is going to be resized less times and thus take less memory.

2. There is no such thing as a perfect data structure. Compare this hash table to the treap you are creating in your data structures class. What are the advantages and disadvantages of both? If you had to use one as the underlying data structure for a map, which one would you choose and why?

In general a treap can have a higher time complexity for search, insert, remove than a hash table. If time is a priority then using a hash table is probably the best option. On the other hand, hash tables usually take a lot of space and depending on the size of your input, collisions could be a real concern that could push you to use other more costly hash functions. 
