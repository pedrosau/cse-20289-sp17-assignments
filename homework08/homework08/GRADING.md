Grading - Homework 08
=====================

Activity 1
----------

- 0.25	Insert is O(n) space worst case
- 0.25	Search is O(n) time worse case
- 0.25	Missing complexities

11.25 / 12

Activity 2
----------

3 / 3


Total
-----

14.25 / 15
