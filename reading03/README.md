Reading 03
==========
Pedro Saunero

1)I would use tr for this. 'echo "All your base are belong to us" | tr a-z A-Z' would convert the characters 
from the lowercase alphabet to the uppercase alphabet.

2)I would use echo ' "monkeys love bananas" | sed 's/monkey/gorillaz/'' for this one. This tells sed to replace
monkeys with gorillaz using / as the delimiter

3) In this one we need to us ' echo "     monkeys love bananas" | sed 's/^ *//' '. We are telling the computer to delete
one or more blank characters at the begining of the line with ^ *.

4) For this it is necessary to use ' cat /etc/passwd | grep :0:0: | cut -d : -f 7 '. First grep outputs the root account because
it always has UID and GID 0. Then we cut part of this line using cut and establishing the delimiter as : and then telling cut to 
print the seventh field.

5) I used ' cat /etc/passwd | sed -r 's_/bin/[batc]+sh_/usr/bin/python_g' | grep python ' for this one. This tells sed to change every line that
contains one or more of (batc) together with sh to the second part of the argument. 

6)I used ' cat /etc/passwd | grep -E '4[[:digit:]]?7' ' for this one. This basically tells grep to look for lines that contain a 4 followed by 
0 or one digit and then a 7.

7) For this we can use ' grep -f f1.txt f2.txt '.

8) We just need to add a -v flag to the command above.



