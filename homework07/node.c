/* node.c */

#include "node.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct node *	node_create(char *string, struct node *next) {
    struct node *newn=malloc(sizeof(struct node));
    if(newn!=NULL){ 
        newn->number=atoi(string);
        newn->string=strdup(string);
        newn->next=next;
        return newn;
    }return NULL;
}


struct node *   node_delete(struct node *n, bool recursive) {
    if(recursive){
        struct node *dummy=n;
        while(dummy!=NULL){
            dummy=dummy->next;
            free(n->string);
            free(n);
            n=dummy;
        }       
    }else{
        free(n->string);
        free(n);
    }
    return NULL;
}


void            node_dump(struct node *n, FILE *stream) {
    fprintf( stream, "Node{%s, %d, %p}\n",n->string,n->number,n->next);   
}


int		node_compare_number(const void *a, const void *b) {
    struct node **c=(struct node **)a;
    struct node **d=(struct node **)b;
    if( (*c)->number < (*d)->number ){
        return -1;
    }else if ( (*c)->number > (*d)->number ){
        return 1;
    }else if( (*c)->number == (*d)->number){
        return 0;
    }
    return 0;
}

int		node_compare_string(const void *a, const void *b) {
    struct node **c=(struct node **)a;
    struct node **d=(struct node **)b;
    return strcmp( (*c)->string, (*d)->string );
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
