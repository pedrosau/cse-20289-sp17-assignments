/* lsort.c */

#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s\n", PROGRAM_NAME);
    fprintf(stderr, "  -n   Numerical sort\n");
    fprintf(stderr, "  -q   Quick sort\n");
    exit(status);
}

void lsort(FILE *stream, bool numeric, bool quicksort) {
    struct list *l= list_create();
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stdin)){
        char *s;
        int len = strlen(buffer)-1;
        if (buffer[len] == '\n')  buffer[len] = '\0';
        s=buffer;
        list_push_front(l, s);
    }
    if(numeric){
        switch(quicksort){
            case 0: list_msort(l, node_compare_number); break; 
            case 1: list_qsort(l, node_compare_number); break;
        }
    }else{
        switch(quicksort){
            case 0: list_msort(l, node_compare_string); break;
            case 1: list_qsort(l, node_compare_string); break;
        }
    }
    struct node *n=l->head;
    while(n!=NULL){
        printf("%s\n",n->string);
        n=n->next;
    }
    list_delete(l);
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    int QS=0;
    int NS=0;
    int argind = 1;
    PROGRAM_NAME = argv[0];
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'h':
                usage(0);
                break;
            case 'n':
                NS=1;
                break;
            case 'q':
                QS=1;
                break;
            default:
                usage(1);
                break;
        }
    }
    /* Sort using list */
    
    lsort(stdin, NS, QS);   
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
