#!/usr/bin/env python2.7

import os
import commands
'''
command='shuf -i1-{} -n {} | ./measure ./lsort -n > /dev/null'
command1='shuf -i1-{} -n {} | ./measure ./lsort -n -q > /dev/null'
o1=commands.getoutput(command.format(1,1)).split()
o11=commands.getoutput(command1.format(1,1)).split()
o2=commands.getoutput(command.format(10,10)).split()
o11=commands.getoutput(command1.format(1,1)).split()
o3=commands.getoutput(command.format(100,100)).split()
o4=commands.getoutput(command.format(1000,1000)).split()
o5=commands.getoutput(command.format(10000,10000)).split()
o6=commands.getoutput(command.format(100000,100000)).split()
o7=commands.getoutput(command.format(1000000,1000000)).split()
o8=commands.getoutput(command.format(1000000,1000000)).split()
print(o1[0],o1[2],o2[0],o2[2],o3[0],o3[2],o4[0],o4[2],o5[0],o5[2],o6[0],o6[2],o7[0],o7[2],o8[0],o8[2])
print('''

command='shuf -i1-{} -n {} | ./measure ./lsort -n > /dev/null'
command1='shuf -i1-{} -n {} | ./measure ./lsort -n -q > /dev/null'
o1=commands.getoutput(command.format(1,1)).split()
o11=commands.getoutput(command1.format(1,1)).split()
o2=commands.getoutput(command.format(10,10)).split()
o22=commands.getoutput(command1.format(10,10)).split()
o3=commands.getoutput(command1.format(100,100)).split()
o33=commands.getoutput(command.format(100,100)).split()
o4=commands.getoutput(command.format(1000,1000)).split()
o44=commands.getoutput(command1.format(1000,1000)).split()
o5=commands.getoutput(command.format(10000,10000)).split()
o55=commands.getoutput(command1.format(10000,10000)).split()
o6=commands.getoutput(command.format(100000,100000)).split()
o66=commands.getoutput(command1.format(100000,100000)).split()
o7=commands.getoutput(command.format(1000000,1000000)).split()
o77=commands.getoutput(command1.format(1000000,1000000)).split()
o8=commands.getoutput(command.format(10000000,10000000)).split()
o88=commands.getoutput(command1.format(10000000,10000000)).split()
print(
'''
| NITEMS   | SORT     | TIME      | SPACE     |
| -------- | -------- |-----------|-----------|
| 1        | Merge    | {}\t| {}\t|
| 1        | Quick    | {}\t| {}\t|
| 10       | Merge    | {}\t| {}\t|
| 10       | Quick    | {}\t| {}\t|
| 100      | Merge    | {}\t| {}\t|
| 100      | Quick    | {}\t| {}\t|
| 1000     | Merge    | {}\t| {}\t|
| 1000     | Quick    | {}\t| {}\t|
| 10000    | Merge    | {}\t| {}\t|
| 10000    | Quick    | {}\t| {}\t|
| 100000   | Merge    | {}\t| {}\t|
| 100000   | Quick    | {}\t| {}\t|
| 1000000  | Merge    | {}\t| {}\t|
| 1000000  | Quick    | {}\t| {}\t|
| 10000000 | Merge    | {}\t| {}\t|
| 10000000 | Quick    | {}\t| {}\t|
'''.format(o1[0],o1[2],o11[0],o11[2],o2[0],o2[2],o22[0],o22[2],o3[0],o3[2],o33[0],o33[2],o4[0],o4[2],o44[0],o44[2],o5[0],o5[2],o55[0],o55[2],o6[0],o6[2],o66[0],o66[2],o7[0],o7[2],o77[0],o77[2],o8[0],o8[2],o88[0],o88[2]) )

