/* list.c */
#include "stdio.h"
#include "list.h"
/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);


struct list *	list_create() {
   struct list *nlist=malloc(sizeof(struct list));
   nlist->head=NULL;
   nlist->tail=NULL;
   nlist->size=0;	
   return nlist;
}

struct list *	list_delete(struct list *l) {
    node_delete(l->head,1);
    free(l);
    return NULL;
}

void		list_push_front(struct list *l, char *s) {
    if(l->size==0){
        struct node *n=node_create(s, l->head);
        l->head=n;
        l->tail=n;
        l->size=l->size+1;
    }else{
        struct node *n=node_create(s, l->head);
        l->head=n;
	l->size=l->size+1;
    } 
}

void		list_push_back(struct list *l, char *s) {
    if(l->size==0){
	struct node *n=node_create(s, NULL);
	l->head=n;
        l->tail=n;
	l->size=l->size+1;
   }else{
	struct node *n=node_create(s, NULL);
	l->tail->next=n;
	l->tail=n;
	l->size=l->size+1;
   }
}


void		list_dump(struct list *l, FILE *stream) {
    struct node *n=l->head;
    while(n!=NULL){
        node_dump(n, stream);
        n=n->next;
    }
}

struct node **	list_to_array(struct list *l) {
    struct node **arr=malloc(sizeof(struct node)*l->size);
    struct node *n=l->head;
    for(int i=0;i<l->size;i++){
        arr[i]=n;
        n=n->next;
    }
    return arr;
}


void		list_qsort(struct list *l, node_compare f) {
	struct node **arr=list_to_array(l);
	qsort( arr, l->size, sizeof *arr, f );
	l->head=arr[0];
	if(l->size>1) l->head->next=arr[1]; 
	l->tail=arr[(l->size)-1];
	l->tail->next=NULL;
	for(int i=1;i<(l->size)-1;i++){
		arr[i]->next=arr[i+1];	
	}
	free(arr);
	
		
}


void		list_reverse(struct list *l) {
	struct node *prev=NULL;
	struct node *curr= l->head;
	l->tail=l->head;
	l->head=reverse(curr,prev);
}

struct node*	reverse(struct node *curr, struct node *prev) {
    	
	if(curr==NULL){
		return prev;
	}else{
		struct node *n=reverse(curr->next,curr);
		curr->next=prev;
		return n;	
	}
}

void		list_msort(struct list *l, node_compare f) {
	l->head=msort(l->head, f);
	struct node *n=l->head;
	for(int i=0;i<((l->size)-1);i++){
		n=n->next;
	}
	l->tail=n;	
}


struct node *	msort(struct node *head, node_compare f) {
	if(head==NULL) return head;
    	if(head->next==NULL){ 
	    return head;	
	}else{
	    struct node *left, *right;
	    split(head,&left,&right);
	    left=msort(left,f);
	    right=msort(right,f);
	    head=merge(left,right,f);
	    return head;
	}
	return NULL;
}
void		split(struct node *head, struct node **left, struct node **right) {
	struct node *fast=head;
	struct node *slow=head;
	struct node *n=head;
	while(fast!=NULL){
	    if(fast->next==NULL) break;
	    slow=slow->next;
	    fast=fast->next->next;
	}
	while(n->next!=slow) n=n->next;
	n->next=NULL;
	*left=head;
	*right=slow;
}


struct node *   merge(struct node *left, struct node *right, node_compare f) {
        struct node *l=left;
        struct node *r=right;
        struct node *h=NULL;
        struct node *t=NULL;
        if(f(&l,&r)<1){
       		t=l;
       		h=t;
       		l=l->next;
        }else{
       		t=r;
       		h=t;
       		r=r->next;
       	}
        while(l!=NULL||r!=NULL){
        	if(l==NULL){
        		t->next=r;
        		t=t->next;
        		r=r->next;
        	}else if(r==NULL){
        		t->next=l;
        		t=t->next;
        		l=l->next;
        	}else{

        		if(f(&l,&r)<1){
        			t->next=l;
        			t=t->next;
        			l=l->next;
        		}else{
        			t->next=r;
        			t=t->next;
        			r=r->next;
        		}

        	}
        }
	t->next=NULL;
        return h;
}
