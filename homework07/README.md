Homework 07
===========

Activity 1)

1. Describe what memory you allocated in node_create and what memory you deallocated in node_delete. How did you handle the 
recursive flag?

In node_create() I allocated enough memory to hold the node struct and,  by using strdup, enough memory to hold the string.
In node_delete() I first free'd the string contained in the node and then the node itself. When recursive is true I created
a dummy node and used a while loop that goes on until the dummy node pointer is NULL. For each iteration of the while loop
I free'd the string and the node. dummy is set to the next node on each iteration.

2. Describe what memory you allocated in list_create and what memory you deallocated in list_delete. How did you handle 
internal struct nodes?

In list_create, I allocated enough memory in the heap to hold a list struc by using malloc. In list delete I not only 
free'd the list itself but also the nodes between the head and the tail pointed by the list by recursively using node_delete.

3. Describe how your list_qsort function works and analyze its time complexity and space complexity in terms of Big-O 
notation (both average and worst case).

list_qsort first creates an array containing the pointers to each node in the list. It then calls the fuction qsort which
returns the sorted array. The fuction finally changes the order of the nodes in the list to mach the one in the array. The 
space complexity is O(n) since it always create an array to be sorted. The time complexity is O(n^2) in the worst case
scenario and on average it is O(nlog n).

4. Describe how your list_reverse function works and analyze its time complexity and space complexity in terms of Big-O notation 
(both average and worst case). 

The reverse function works by using two node pointers: current and previous. It is initially called with current pointing
to the head of the list and previous pointing to NULL. The fuction recursively calls itself until the current node is NULL.
This indicates that the end of the lsit has been reached so the order of the pointers starts being reversed from the tail
to the head by doing: current->next = previous. The space complexity is O(n) since the number of elements in the list increases
the number of variables created each time the function is recursively called. The time complexity in average and in the worst
case scenario is always O(n) because the number of times the function is called linearly increases with the number of elements
to be reversed. 

5. Describe how your list_msort function works and analyze its time complexity and space complexity in terms of Big-O notation 
(both average and worst case).

list_msort calls the function msort and passes a pointer to the head of the list to be sorted. msort returns the pointer passed if 
the end of the list has been reached. if it hasn't msort divides the list in two halves using split, then it calls itself and passes 
the pointers to the heads of the two halves of the divided list. It finslly merges the two halves. This is where the real sorting 
happens. merge compares the heads of the two halves and links them in the correct order until all nodes have been placed.

This functiohas a space complexity of O(n) since the function uses recursion and the number of times the function is called depends
on the size of the list. This increases the memory usage. The time complexity in both the average and worst case scenarios is O(n log n).

Activity 2)

1. Based on your experiments, what is the overall trend in terms of time and space as the number of items increases for both of your 
sorting methods? Are the results surprising to you? Explain why or why not.

As expected, both the time and space increase as the input increases but time seems to increase exponentially while space linearly.
Qsort seems to always be faster than msort but it does take more space. These results go along the predicted time and space complexities.
The only surprising part is that qsort is always faster than msort, even with an input of 10000000 numbers since we qsort can be O(n^2) while
msort is always O(nlogn). 


2. Given what you said about the space complexity and time complexity of list_msort and list_qsort in Activity 1 and the experimental results 
in Activity 2, what can you say is the relationship between theoretical complexity and real world performance? Explain.

Theoretical complexity is a prediction of trend, or what can the complexity possibly be as the input increases, not a rule. The fact that qsort
performed better than msort time-wise even though it had a worst-case time complexity of O(n^2) depends on other factors, such as the actual input
and the performance of the computer.

BENCHMARK TABLE:

| NITEMS   | SORT     | TIME      | SPACE     |
| -------- | -------- |-----------|-----------|
| 1        | Merge    | 0.000999	| 0.476562	|
| 1        | Quick    | 0.000999	| 0.476562	|
| 10       | Merge    | 0.000999	| 0.472656	|
| 10       | Quick    | 0.000999	| 0.480469	|
| 100      | Merge    | 0.000999	| 0.488281	|
| 100      | Quick    | 0.000999	| 0.476562	|
| 1000     | Merge    | 0.000999	| 0.527344	|
| 1000     | Quick    | 0.000999	| 0.621094	|
| 10000    | Merge    | 0.007998	| 1.082031	|
| 10000    | Quick    | 0.006998	| 1.289062	|
| 100000   | Merge    | 0.106983	| 6.574219	|
| 100000   | Quick    | 0.088986	| 8.152344	|
| 1000000  | Merge    | 1.877714	| 61.503906	|
| 1000000  | Quick    | 1.334796	| 77.734375	|
| 10000000 | Merge    | 31.524206	| 610.828125	|
| 10000000 | Quick    | 18.430197	| 764.699219	|
