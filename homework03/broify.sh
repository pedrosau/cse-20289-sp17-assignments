#!/bin/bash

if [[ $1 = -h ]] ; then
cat <<EOF
Usage: broify.sh

  -d DELIM    Use this as the comment delimiter.
  -W          Don't strip empty lines.
EOF
exit 0
fi

DELIM=#
PART2=1
for i in 1 2 3 ; do
	if [[ ${!i} = '-d' ]] ; then
		(( TEMP = $i + 1 ))
		TEMP2=$TEMP
		DELIM=${!TEMP2}	
	elif [[ ${!i} = '-W'  ]] ; then
		PART2=0
	fi
done

if [[ $PART2 = 1 ]] ; then
	sed -r "s|[ \t]*$DELIM.*||;/^\s*$/d"
	exit 0
elif [[ $PART2 = 0 ]] ; then
	sed -r "s|[ \t]*$DELIM.*||"
	exit 0
fi

 
exit 0
