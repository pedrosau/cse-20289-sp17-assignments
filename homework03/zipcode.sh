#!/bin/bash
CITY=All
STATE=Indiana
FORMAT=text
if [[ $1 = '-h' ]] ; then
cat <<EOF
Usage: zipcode.sh

  -c      CITY    Which city to search
  -f      FORMAT  Which format (text, csv)
  -s      STATE   Which state to search (Indiana)

If not CITY is specified, then all the zip codes for the STATE are displayed.
EOF
exit 0
fi

for i in 1 2 3 4 5 6 ; do
        if [[ ${!i} = '-s' ]] ; then
                (( TEMP = $i + 1 ))
                STATE=`echo ${!TEMP} | sed 's| |%20|'`
		
		#echo $STATE
		#exit 0 
        elif [[ ${!i} = '-c'  ]] ; then
                (( TEMP = $i + 1 ))
		CITY=${!TEMP} 
        elif [[ ${!i} = '-f'  ]] ; then
                (( TEMP = $i + 1 ))
                FORMAT=${!TEMP} 
	fi
done

ABREV=`curl -s http://www.zipcodestogo.com/$STATE/ | grep href=\"http://www\.zipcodestogo\.com/[[:alpha:]]*/[[:upper:]]*/[[:digit:]] | cut -d '/' -f 5 | uniq`

if [[ $FORMAT = csv ]] ; then
	if [[ $CITY = 'All' ]] ; then
                curl -s http://www.zipcodestogo.com/${STATE}/ | grep "/${ABREV}/[[:digit:]*]" | cut -d '/' -f 6 | paste -sd ","
        else
                curl -s http://www.zipcodestogo.com/${STATE}/ | grep "/${ABREV}/[[:digit:]*]" | grep "/${CITY}/" | cut -d '/' -f 6 | paste -sd ","
        fi
else
	if [[ $CITY = 'All' ]] ; then 
		curl -s http://www.zipcodestogo.com/${STATE}/ | grep "/${ABREV}/[[:digit:]*]" | cut -d '/' -f 6
	else
		curl -s http://www.zipcodestogo.com/${STATE}/ | grep "/${ABREV}/[[:digit:]*]" | grep "/${CITY}/" | cut -d '/' -f 6
	fi
fi
