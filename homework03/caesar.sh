#!/bin/bash

NUM='^[0-9]+$'
KEY=13

if [[ $1 == "-h" ]] ; then
	echo "Usage: caesar.sh [rotation]" 
	echo " "
	echo "This program will read from stdin and rotate (shift right) the text by"
	echo "the specified rotation.  If none is specified, then the default value is 13."
	exit 0
elif [[ $1 =~ $NUM ]] ; then
	KEY=$1
fi

if [[ $KEY -gt 26 ]] ; then
	(( KEY = $KEY % 26 ))
fi 
CAP[1]=A
CAP[2]=B
CAP[3]=C
CAP[4]=D
CAP[5]=E
CAP[6]=F
CAP[7]=G
CAP[8]=H
CAP[9]=I
CAP[10]=J
CAP[11]=K
CAP[12]=L
CAP[13]=M
CAP[14]=N
CAP[15]=O
CAP[16]=P
CAP[17]=Q
CAP[18]=R
CAP[19]=S
CAP[20]=T
CAP[21]=U
CAP[22]=V
CAP[23]=W
CAP[24]=X
CAP[25]=Y
CAP[26]=Z
LOW[1]=a
LOW[2]=b
LOW[3]=c
LOW[4]=d
LOW[5]=e
LOW[6]=f
LOW[7]=g
LOW[8]=h
LOW[9]=i
LOW[10]=j
LOW[11]=k
LOW[12]=l
LOW[13]=m
LOW[14]=n
LOW[15]=o
LOW[16]=p
LOW[17]=q
LOW[18]=r
LOW[19]=s
LOW[20]=t
LOW[21]=u
LOW[22]=v
LOW[23]=w
LOW[24]=x
LOW[25]=y
LOW[26]=z

SET1=\'[A-Za-z]\'
(( MID =  $KEY  ))
(( TEMP = MID + 1 ))
SET2=\'[${CAP[${TEMP}]}-ZA-${CAP[$MID]}${LOW[${TEMP}]}-za-${LOW[$MID]}]\'

#read TXT

#echo $SET2
#echo ${TXT} | 
 tr ${SET1} ${SET2}

