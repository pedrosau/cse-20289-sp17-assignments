Homework 03
===========

PEDRO SAUNERO

Activity 1)

1. I did it by using an if statement checking if $1 was equal to -h. If so a message was displayed. Then I use an if statement
to determine if $1 was a digit and if so, it was set as the key.
2. The source set is always the same: [A-Za-z]. I chose this to always read both upper and ower case letters.
3. Setting SET2 was more complex.I first created two arrays: one for capital letters CAP[x] and one for lower case
 letters LOW[x]. I created a variable called TEMP and set it to the KEY plus 1. Then I constructed the set by placing
a string with the following pattern inside brackets: CAP[TEMP]-ZA-CAP[KEY]LOW[TEMP]-za-LOW[KEY]. So if the key is
13 you would get [N-ZA-Mn-za-m].
4. The last step was to use those sets for encription. For this, i simply used tr $SET1 $SET2 so the input is transformed
from the first set to the second.

Activity 2)

1. First, I checked $1 to see if it was -h. After that I used a for loop that went from 1 to 3 and checked whether 
if the argument was -W or -d. If it was -g, the program checks the following argument to search for the DELIMITER.
2. I used sed -r "s|[ \t]*$DELIM.*||". This removes any string that may start with white space followed by the delimiter
and anything that follows that delimiter.
3.I removed emty lines by using sed '/^\s*$/d'. This tells the program to delete any line that starts and ends with white
space.
4.The delimiter was set to be #. However, if an option was present then the variable was changed accordingly. In the line 
where the sed command is executed, the delimiter is present in the form of a variable. If the option -W was detected then
a shorter of the command sed was executed and it didn't include the part to remove blank lines.
*/
Activity 3)

1.I parsed the command line arguments by first checking if the first argument was -h. If so, I displayed the usage message.
Then I used a for loop from 1 to 6 (max number of arguments/flags allowed). With that loop I checked if argument x was one of the flags
and if so, I saved the next argument as the appropiate variable. 
2.I extarcted by first using curl to get the info from the appropiate state.Then I used grep and cut to get the state abbreviation (IN).
 Then I used grep to get the lines that contain a zip code somewhere in them with the argument "/${ABREV}/[[:digit:]*]" so, for example, 
/IN/46556 would be detected. Then I used cut withthe delimiter set to '/' and got the 6th field from it. This field contained the zip codes.
3. If the flags -s or -c were detected, I set the variables $STATE and $CITY to the argument next to the flag, although for city
I first needed to use sed to replace any spaces with %20 because the variable was used in an URL. After getting those variables it
was just a matter of replacing them in the required fields: the URL used with curl for $CITY and grep filter for $CITY.
4.The text format was set as a default in the variable $FORMAT. If the -f csv arguments were detected then the variable $FORMAT
was changed to csv. When executing the final commands, I used an if statement to check if $FORMAT was csv and if so, it executed
a longer command that added paste -sd "," to transform the output into a csv.
