Homework 04
===========

Activity 1)

-How you parsed command line arguments.
 I parsed the arguments by using a while loop that looped while the argument started with '-'  and there were arguments left.
For each iteration of the loop, I popped the argument and used an if statement to determine what to do with it. For -d and
-s I popped the next argument to get the delay and stepsize values. 

How you managed the temporary workspace.

I created a temporary workspace using tempfile.mkdtemp() and deleted it when the program was closing using shutil.rmtree(workspace).

How you extracted the portrait URLS.

Once I had the profile webpage after getting it with requests.get, I searched for the URL by getting the part of the line in the code 
that contains it. I needed to use re.findall($PROFILEURL + '/@@images[^\"]+',PROF1.text). This searches for a line with a link that 
includes https://engineering.nd.edu/profiles/$NETID... up until it encounters a ' " ' char and returns the URL

How you downloaded the portrait images.
									
I downloaded the images using, once again, requests.get($IMAGEURL) and then created a file with the the downloaded information
using the write function.

How you generated the blended composite images.

After getting the two images to be blended, I used a for loop that went from 0 to 100 in $STEPSIZE intervals and for each iteration 
I used os.system('composite -blend '+ $ITERATOR + ' ' + $IMAGE1 + ' ' + $IMAGE2 + ' ' + output). This sends the composite command
to the system using the current value of the iterator as the stepsize and the names of the two image files created as sources.

How you generated the final animation.

To create the final animation I made a list with the name of all the blended images created. Then I sent a command to the system 
using os.system() with the command 'convert -loop 0 -delay $DELAY ' concatenated with the list with the filenames extended as
one string to be used as arguments. To extend the list into one string I used " ".join.  

How you checked for failure of different operations and exited early.

I used try: except: to try and get the profile URL. If the URL doesn't exist, an exit code is sent.


Activity 2)

How you parsed command line arguments.

Similarly to blend.py I used a while loop that runs while there is more than 0 arguments left and while the next argument starts
with '-'. For each iteration of the loop, I popped the next argument and stored it as the FIELD, SUBREDDIT or LIMIT depending on the argument.
If only one argument is left after this, it is used as the string to be used to search in the field specified.

How you fetched the JSON data and iterated over the articles.

I fetched the json data by using requests.get with the URL to the subreddit specified. After that, I used re.findall to get the titles,
authors and links to the entries on the subreddit.

How you filtered each article based on the FIELD and REGEX.

To search for the articles that contain a string on their FIELD field I used re.compile(STRING_TO_BE_SEARCHED) and then used filter() on the list containing
the titles, authors or links. For each element in the filterd list, I got its index number in the old list and created a new list for each of the other fields
containig only the members with the index numbers of the filtered list's members. This way, the results of the filtered list match with the other fields.

How you generated the shortened URL.

I used a for loop that iterated over the list containig the links and  used r=requests.get("http://is.gd/create.php", params = 
{ 'format' : 'json', 'url': 'https://www.reddit.com'+str(link) } ) and then cut the part of the string returned that contains the short URL.








