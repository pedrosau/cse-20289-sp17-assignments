#!/usr/bin/env python2.7


import atexit
import os
import re
import shutil
import sys
import tempfile

import requests

os.environ['PATH'] = '~ccl/software/external/imagemagick/bin:' + os.environ['PATH']

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-r':
        REVERSE = True
    elif arg == '-d':
        DELAY = int(args.pop(0))
    elif arg == '-s':
        STEPSIZE = int(args.pop(0))
    elif arg == 'h':
        usage(0)

if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]


workspace = tempfile.mkdtemp()
print('Using workspace: '+str(workspace))

def cleanup():
	print 'Cleaning up workspace: ',str(workspace)
	shutil.rmtree(workspace)

atexit.register(cleanup)


PURL=['','']
IURL=['','']
PURL[0]='https://engineering.nd.edu/profiles/'+netid1
PURL[1]='https://engineering.nd.edu/profiles/'+netid2

try:
	PROF1=requests.get(PURL[0])
	PROF2=requests.get(PURL[1])
except:
	sys.exit(1)

def get_extension(filename):
	ext=filename.rsplit('.', 1)[-1]
	return ext

temp1=re.findall(PURL[0]+'/@@images[^\"]+',PROF1.text)
temp2=re.findall(PURL[1]+'/@@images[^\"]+',PROF2.text)

print 'Searching portrait for ',netid1,'... ',
IURL[0]=str(temp1[0])
print IURL[0]
print 'Searching portrait for ',netid2,'... ',
IURL[1]=str(temp2[0])
print IURL[1]

print 'Downloading ',IURL[0],'... ',
image1=requests.get(IURL[0])
print 'Success!'
print 'Downloading ',IURL[1],'... ',
image2=requests.get(IURL[1])
print 'Success!'

f1 = open('image1.'+get_extension(IURL[0]),'w')
f1.write(image1.content)
f1.close()

f2 = open('image2.'+get_extension(IURL[1]),'w')
f2.write(image2.content)
f2.close()

def blend(source1 , source2, stepsize, output):
	os.system('composite -blend '+ stepsize+' '+ source1+' '+ source2+' '+output)	

files = []
for step in range( 0 , 101 , STEPSIZE):
	print 'Generating ', str(workspace), format(step,"03d")+'-'+target, '... ',
	blend('image1.'+get_extension(IURL[0]), 'image2.'+get_extension(IURL[1]), str(step) ,format(step,"03d")+'-'+target)
	files.append(format(step,"03d")+'-'+target)
	print('Success!')

files.append(target)

print 'Generating ', target, ' ... ',

if REVERSE:
	temp=files[::-1]
	os.system('convert -loop 0 -delay '+str(DELAY)+' '+" ".join(files[:-1])+' '+" ".join(temp[1:])+' '+files[-1])
else:
	os.system('convert -loop 0 -delay '+str(DELAY)+' '+" ".join(files))
print 'Success!'




