#!/usr/bin/env python2.7

import requests
import re
import sys
import os

FIELD='title'
LIMIT=10
SUBREDDIT='linux'
SEAR=False

def usage(status=0):
    print '''Usage: {} [ -f FIELD -s SUBREDDIT ] regex
    -f FIELD        Which field to search (default: {})
    -n LIMIT        Limit number of articles to report (default: {})
    -s SUBREDDIT    Which subreddit to search (default: {})'''.format(
        os.path.basename(sys.argv[0]), FIELD, LIMIT, SUBREDDIT
    )
    sys.exit(status)

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-f':
        FIELD = str(args.pop(0))
        if FIELD != 'title' and FIELD != 'author' and FIELD != 'link':
            print 'Invalid field: ',FIELD
            sys.exit(1)
    elif arg == '-n':
        LIMIT = int(args.pop(0))
    elif arg == '-s':
        SUBREDDIT = str(args.pop(0))
    elif arg == '-h':
        usage(1)

if len(args) == 1:
	SEAR=True
	SEARCH=str(args.pop(0))

URL='https://www.reddit.com/r/'+SUBREDDIT+'/.json'
headers  = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
response = requests.get(URL, headers=headers)

titles= re.findall(r"\"title\": \"([^\"]+)\", \"created_utc", response.text)
authors=re.findall(r"\"author\": \"([^\"]+)", response.text)
links=re.findall(r"\"permalink\": \"([^\"]+)\", \"num_reports", response.text)
shlinks=[]

for link in links:
	r=requests.get("http://is.gd/create.php", params = { 'format' : 'json', 'url': 'https://www.reddit.com'+str(link) } )
	short=re.findall('http[^\"]+',r.text)
	shlinks.append(str(short[0]))

if SEAR==False:
	for num in range(1,int(LIMIT)+1):
		print ''
		print (str(num)+'.'),' Title:\t',str(titles[int(num)-1])
		print '    Author:\t', str(authors[int(num)-1])
		print '    Link:\t', ('https://www.reddit.com'+str(links[int(num)-1]))
		print '    Short:\t', shlinks[int(num)-1]

else:
	TO_SEARCH = re.compile(".*"+SEARCH + ".*")
	if FIELD=='title':
		otitles=titles
		nauthors=[]
		nlinks=[]
		nshlinks=[]
		titles=filter(TO_SEARCH.match,titles)
		if len(titles)<LIMIT:
			LIMIT=len(titles)
		for i in titles:
			index=otitles.index(str(i))
			nauthors.append(authors[index])
			nlinks.append(links[index])
			nshlinks.append(shlinks[index])
		authors=nauthors
		links=nlinks
		shlinks=nshlinks
	elif FIELD=='author':
		oauthors=authors
		ntitles=[]
		nlinks=[]
		nshlinks=[]
		authors=filter(TO_SEARCH.match,authors)
		if len(authors)<LIMIT:
			LIMIT=len(authors)
		for i in authors:
			index=oauthors.index(str(i))
			ntitles.append(titles[index])
			nlinks.append(links[index])
			nshlinks.append(shlinks[index])
		titles=ntitles
		links=nlinks
		shlinks=nshlinks
	elif FIELD== 'link':
		olinks=links
		ntitles=[]
		nauthors=[]
		nshlinks=[]
		links=filter(TO_SEARCH.match,links)
		if len(links)<LIMIT:
			LIMIT=len(links)
		for i in links:
			index=olinks.index(str(i))
			ntitles.append(titles[index])
			nauthors.append(authors[index])
			nshlinks.append(shlinks[index])
		titles=ntitles
		authors=nauthors
		shlinks=nshlinks
	else:
		print 'Invalid field'
		sys.exit(1)

	for num in range(1,int(LIMIT)+1):
		print ''
		print (str(num)+'.'),' Title:\t',str(titles[int(num)-1])
		print '    Author:\t', str(authors[int(num)-1])
		print '    Link:\t', ('https://www.reddit.com'+str(links[int(num)-1]))
		print '    Short:\t', shlinks[int(num)-1]

	
