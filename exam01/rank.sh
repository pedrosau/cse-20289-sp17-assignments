#!/bin/sh

NUM=5
DES=0
dispusage(){

cat <<EOF
Usage: rank.sh [-n N D]

        -n N    Returns N items (default is 5).
        -D      Rank in descending order
EOF
exit 0
}

while [ $# -gt 0 ] ; do
        case $1 in
                -h) dispusage ;;
                -n) NUM=$2 shift;;
                -D) DES=1 ;;
        esac
        shift
done

if [[ $DES = 0 ]] ; then
        sort -n | head -n $NUM
elif [[ $DES = 1 ]] ; then
        sort -n -r | head -n $NUM
fi
exit 0

