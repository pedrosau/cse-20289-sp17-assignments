GRADING - Exam 01
=================

- Commands:         2.25
- Short Answers:
    - Files:        2.75 
    - Processes:    2.75 
    - Pipelines:    2.75
- Debugging:        2.75
- Code Evaluation:  2 
- Filters:          3 
- Scripting:        3 
- Total:	    21.25
