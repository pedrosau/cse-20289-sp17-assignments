#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
    size_t i = 0;
    
    while (i < n && scanf("%d", &numbers[i]) != EOF) {
        i++;
    }
    
    return i;
}



void sort_numbers(int numbers[], size_t n) {
    
    int j,k;
    for (int i = 1; i < n; i++){
        k = numbers[i];
        j = i-1;
        
        while (j >= 0 && numbers[j] > k){
            numbers[j+1] = numbers[j];
            j = j-1;
        }
        numbers[j+1] = k;
    }

}

int main(int argc, char *argv[]) {
    int numbers[MAX_NUMBERS];
    size_t nsize;
    
    nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
    if (nsize!=0){
        sort_numbers(numbers, nsize);
        printf("%d", numbers[0]);
        for(int i=1;i<nsize;i++){
            printf(" %d", numbers[i]);
        }
    }   
     printf("\n");
    return EXIT_SUCCESS;
}
