Reading 07
==========

1. Describe what the read_numbers function does by explaining what is happening on the following line:
while (i < n && scanf("%d", &numbers[i]) != EOF) {

This while loop scans the next element in stream and, if it is a decimal, it stores the element in number[i] until 
the end of the stream is reached (denoted by EOF). & in &numbers[i] is used to get the address of the (i-1)th element
in the numbers array.

2. This won't work because numbers in sizeof(numbers) is an array so sizeof won't return the size of the array but rather 
the size of the pointer to the array (numbers*)

---------
1. Compare how this version of cat parses command line arguments versus the approach used in cat.py from Reading 05. Beyond 
the obvious differences in syntax, how is the code similar and how is it different? 

argc is a variable that contains the number of arguments passed in the command line (including the executable). The program goes through
the arguments by comparing a variable containing the index of the argv array (which contains the arguments) argind and  argc. For each iteration of the
while loop used, the program ensures that the next argument has a length > 1 and that it starts with '-'. Instead of popping arguments from the array 
like we did in python, the argind value is increased in each iteration to point to the next argument, while the current argument is stored as
a string (using a char pointer).

2. Describe how each file is processed by explaining the following code:

while (argind < argc) {
    char *path = argv[argind++];
    if (strcmp(path, "-") == 0) {
        cat_stream(stdin);
    } else {
        cat_file(path);
    }
}

The loop goes through each argument containing a file name or '-' by soring the argind member in argv in a C-string path and then incrementing argind.
For file names cat_file is called and for a '-' argument cat_stream is called.

3. Explain how the cat_stream function works. What are the purposes of fgets and fputs?

The function creates a string (or actually a char array) buffer and stores (BUFSIZ-1) elements from the stream in the buffer by using
fgets. After doing this, yyhe function writes the buffer to stdout using fputs.

4. Explain how the cat_file function works. What is the purpose of the following if statement:

if (fs == NULL) {
    fprintf(stderr, "%s: %s: %s\n", PROGRAM_NAME, path, strerror(errno));
    return;
}

This function creates a stream from a file using fopen and passes that stream to  cat_stream to display the text.
The if statement is used in case the file is not found. It prints the program name, the filenme not found and a description 
of the error number returned by errno passed to sterror.

 
