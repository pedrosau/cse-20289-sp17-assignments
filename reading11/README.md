Reading 11
==========

1. Creating a file descriptor or endpoint for communicating over the network.

int file_descriptor = socket( domain, type, protocol);

2. Lookup the address information for a particular machine or service.

getaddrinfo(node, service, &hints, res);

3. Assign an address to a server network file descriptor (aka assign a name to a socket).

bind(sock, addr, addr_len);

4. Convert the network address structure into corresponding host and service strings.

for(p = infoptr; p != NULL; p = p->ai_next) {

    getnameinfo(p->ai_addr, p->ai_addrlen, host, sizeof(host), service, sizeof(service), NI_NUMERICHOST);
    puts(host);
 }

5. Establish a client network file descriptor to a specified address.

connect(sockfd, addr, addrlen);

6. Mark network file descriptor as waiting for incoming connections.

listen(int sockfd, int backlog);

7. Create a new network file descriptor based on a incoming connection.

new_fd = accept(fd, addr, addrlen);
