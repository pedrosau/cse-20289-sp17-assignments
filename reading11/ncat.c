//ncat.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

void cat(int socket_fd) {
    char buffer[BUFSIZ];
    while (fgets(buffer, BUFSIZ, stdin)) {
        write(socket_fd, buffer, strlen(buffer));
    }

}


int main(int argc, char **argv){
    int socket_fd = -1;
    int rv;
    // Parse command line options
    char *host, *port;
    if(argc == 3){
        host = argv[1];
        port = argv[2];
    }else{
        fprintf(stderr,"Usage: %s HOST PORT", argv[0]);
        return 1;
    }

    //Create socket and connect to specified HOST and PORT
    struct addrinfo *results, hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family   = AF_INET;
    hints.ai_socktype = SOCK_STREAM;


    if ((rv=getaddrinfo(host, port, &hints, &results)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket");
        return 1;
    }


    for (struct addrinfo *p = results; p != NULL ; p = p->ai_next) {

        if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
         	perror("Connect");
    		close(socket_fd);
    	    continue;
        }
        cat(socket_fd);

        break;

    }
    freeaddrinfo(results);
    shutdown(atoi(port), SHUT_RDWR);
    close(socket_fd);


   return 0;
}
