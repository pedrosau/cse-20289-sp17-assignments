reading05 - Grade
=================

**Score**: 3.5/4

Deductions
----------
-0.25 - exists.py does not exit with appropriate status
-0.25 - head.py does not have a correct usage message

Comments
--------
Good work!