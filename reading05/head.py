#!/usr/bin/env python2.7

import os
import sys

# Global Variables

NUM = 10

# Usage function

def usage(status=0):
    print '''Usage: {} files...

    -E  display $ at end of each line'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-n':
        temp = args.pop(0)
        NUM = temp
    else:
        usage(1)

if len(args) == 0:
    args.append('-')

# Main execution

for path in args:
    if path == '-':
        stream = sys.stdin
    else:
        stream = open(path)
    
    count = 0
    
    for line in stream:
        
        if count != int(NUM):
            line = line.rstrip()
            print line
            count = count + 1

    stream.close()
