#!/usr/bin/env python2.7

import sys
import os

for arg in sys.argv[1:]:
	if os.path.isfile(arg):
		print arg, 'exists!'
	else:
		print arg, 'does not exist!'

