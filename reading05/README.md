Reading 05
==========

1) import sys imports the sys module to the script so variables and functions defined in the module can be also used in the script.
for arg in sys.argv[1:]: is a loop that goes through the arguments passed when calling the script from the second to the last one. The
trailing , is used to prevent a new line from being emitted.

2) a) A list args container is first created with all the arguments passed (except the name of the script). 
The loop 'while len(args) and args[0].startswith('-') and len(args) > 1' works in three ways. First, len(args) 
is interpreted as True for all values except 0 so as long as the length of args is greater than 0 that part will
be true. The length is eventually 0 because the elements of the list are popped.  args[0].startswith('-') checks 
if the argument starts with '-' to make sure it is reading a flag instead of a file name. 

b) if len(args) == 0:
    args.append('-')
 
This block inserts a '-' character in the list args if it is currently empty.

if path == '-':
    stream = sys.stdin
else:
    stream = open(path)

This block thells the program to concatenate the stream from the standard input if no files have been specified 
when (when '-' has been added to args) and set stream to the file specified in args if a file is specified.

c) rstrip removes the trailing white spaces (if any). It might be necessary depending on the size of the screen 
the program is executed on as trailing white spaces might mess with the formating.


