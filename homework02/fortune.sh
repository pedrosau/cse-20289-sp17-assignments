#!/bin/sh

PATH=/afs/nd.edu/user15/pbui/pub/bin:$PATH

trap 'cowsay Leaving so soon?; exit 0' 1 2 15

M[0]="It's certain"
M[1]="It is decidedly so"
M[2]="Without a doubt"
M[3]="Yes, definitely"
M[4]="You may rely on it"
M[5]="As I see it, yes"
M[6]="Most likely"
M[7]="Outlook good"
M[8]="Yes"
M[9]="Signs point to yes"
M[10]="Reply hazy try again"
M[11]="Ask again later"
M[12]="Better not tell you now"
M[13]="Cannot predict now"
M[14]="Concentrate and ask again"
M[15]="Don't count on it"
M[16]="My reply is no"
M[17]="My sources say no"
M[18]="Outlook not so good"
M[19]="Very doubtful"

P[0]="Hello $USER, what question do you have for me today?"
P[1]="Hi $USER, what can I do for you?"
P[2]="Why $USER, what is on your mind?"


  

  R1=`shuf -i 0-2 -n 1`
  cowsay ${P[${R1}]}

  QUE=""
  while [[ $QUE = "" ]]
  do
	read -p "Question? " QUE 
  done
  
  R2=`shuf -i 0-19 -n 1`
  cowsay ${M[${R2}]}
  


  
 




