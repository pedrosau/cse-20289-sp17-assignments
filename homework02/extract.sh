#!/bin/sh

if [ $# -eq 0 ]; then
    echo "Usage: extract.sh archive1 archive2..."
    exit 1
fi

for file in $@
do

  case $file in
 	*.tar.gz)
	  tar zxvf "$file"
	  break
	  ;;	
	*.tgz)
          tar zxvf "$file"
          break
          ;;  
	*.tar.bz2)
	  tar xvjf "$file"
	  break
          ;;
	*.tbz)
          tar xvjf "$file"
          break
          ;;
	*.jar)
	  unzip  "$file"
	  break
	  ;;
	*.zip)
          unzip  "$file"
          break
          ;;
	*.tar.xz)
	  tar xJvf  "$file"
	  break
	  ;;
	*.txz)
          tar xJvf  "$file"
          break
          ;;
	 

  esac

done
