Homework 02
===========


Activity 1)

	1. I used an IF statement to test if the number of arguments provided was equal to zero. If true a message is displayed and the 
	program exits with code 1.

	2. Most arguments needed a tar command and after that it was a matter of reading through the guide to check what options were 
	needed to uncompress the file. x and f were always options because they extract and check for the file name.

	3. The most challenging aspect of writing the script was getting the right options for each command. This was simply solved
	by reading the guides on tar and unzip.

	the program is the following:
       
____________________________________________________________________________________________________

Activity 2)
	
	1. I created two arrays. One containing the different prompts to the user that the cow can display and the
	other containing all the answers to the questions. I created two variables that were created randomly using
	shuf. One can go from 0-2 for the prompts and the other one from 0-19 for the answers. Finally I just used 
	cowsay ${ARRAY[${RANDOM}]}  to display the random message.

	2. To handle signals I used trap 'cowsay "MESSAGE"; exit 0' 1 2 15. What this does is catch the signals 1 2 and 15
 	and wen it detects one of them it runs cowsay MESSAGE and then exits with error code 0.

	3. I read input from the user simply using read -p "Question? " QUE. This displays 'Question? ' and wait for the user's input.
	Then using an IF statement I check if the input is blank. If it is then the user is prompted again. This is done using a 
	WHILE loop until QUE is not blank.

	4. The most challenging part of this program was understanding what altering PATH meant and how it could be implemented to use cowsay.
	I read on the subject and finally understood how to use it. The next challenging part was to figure out a way to choose a random
	message. I decided to use an array because I'm more confortable with them rather than another method.

                                                           

______________________________________________________________________________________
Activity 3)

1. My first step was to scan `xavier.h4x0r.space` for a HTTP port:

        $ nmap -Pn -p 9000-9999 xavier.h4x0r.space
        Starting Nmap 5.51 ( http://nmap.org ) at 2017-02-02 20:09 EST
	Nmap scan report for xavier.h4x0r.space (129.74.160.130)
	Host is up (0.00062s latency).
	Not shown: 997 closed ports
	PORT     STATE SERVICE
	9097/tcp open  unknown
	9111/tcp open  DragonIDSConsole
	9876/tcp open  sd

As can be seen, there are 3 ports in the `9000` - `9999` range.

2. Next, I tried to access the port 9876 using curl 

        $ curl xavier.h4x0r.space:9876
        ________________________________________ 
	/ Halt! Who goes there?                  \
	|                                        |
	| If you seek the ORACLE, you must come  |
	| back and _request_ the DOORMAN at      |
	| /{NETID}/{PASSCODE}!                   |
	|                                        |
	| To retrieve your PASSCODE you must     |
	| first _find_ your LOCKBOX which is     |
	| located somewhere in                   |
	| ~pbui/pub/oracle/lockboxes.            |
	|                                        |
	| Once the LOCKBOX has been located, you |
	| must use your hacking skills to        |
	| _bruteforce_ the LOCKBOX program until |
	| it reveals the passcode!               |
	|                                        |
	\ Good luck!                             /
	 ---------------------------------------- 
	    \                                  ___-------___
	     \                             _-~~             ~~-_
	      \                         _-~                    /~-_
             /^\__/^\         /~  \                   /    \
           /|  O|| O|        /      \_______________/        \
          | |___||__|      /       /                \          \
          |          \    /      /                    \          \
          |   (_______) /______/                        \_________ \
          |         / /         \                      /            \
           \         \^\\         \                  /               \     /
             \         ||           \______________/      _-_       //\__//
               \       ||------_-~~-_ ------------- \ --/~   ~\    || __/
                 ~-----||====/~     |==================|       |/~~~~~
                  (_(__/  ./     /                    \_\      \.
                         (_(___/                         \_____)_)


3. I then went to ~pbui/pub/oracle/lockboxes with cd and found my lockbox
	
	$ find -name "*.lockbox"
	./6e627427/b0dfc4ea/c519d16a/48521642/psaunero.lockbox

4. After this I went into the directory of my lockbox. To test possible passwords I decided to make a script but couldn't create files in 
that directory so I copied my lockbox to my home directory and there I created the following script brute.sh:

#!/bin/sh

for i in $@
do
        psaunero.lockbox $i
done

5. I executed this using strings and a pipeline

	$ strings psaunero.lockbox | xargs -0 brute.sh

	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	7def6291827392fd1821cff8ea54f860
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password
	Usage: lockbox password


6. The password is clearly 7def6291827392fd1821cff8ea54f860 so then I tried to find the doorman by accesing the given address

	$ curl xavier.h4x0r.space:9876/psaunero/7def6291827392fd1821cff8ea54f860
 
	___________________________________ 
	/ Ah yes, psaunero... I've been waiting   \
	| for you.                                |
	|                                         |
	| The ORACLE looks forward to talking to  |
	| you, but you must authenticate yourself |
	| with our agent, BOBBIT, who will give   |
	| you a message for the ORACLE.           |
	|                                         |
	| He can be found hidden in plain sight   |
	| on Slack. Simply send him a direct      |
	| message in the form "!verify netid      |
	| passcode". Be sure to use your NETID    |
	| and the PASSCODE you retrieved from the |
	| LOCKBOX.                                |
	|                                         |
	| Once you have the message from BOBBIT,  |
	| proceed to port 9111 and deliver the    |
	| message to the ORACLE.                  |
	|                                         |
	| Hurry! The ORACLE is wise, but she is   |
	\ not patient!                            /
	 ----------------------------------------- 
	  \
	   \          .
	       ___   //
	     {~._.~}// 
	      ( Y )K/  
	     ()~*~()   
	     (_)-(_)   
	     Luke    
     	Sywalker
     	koala  

7. I finally went to visit the Oracle by using nc

	$ nc xavier.h4x0r.space 9111

	 ________________________ 
< Hello, who may you be? >
 ------------------------ 
    \
     \
                                   .::!!!!!!!:.
  .!!!!!:.                        .:!!!!!!!!!!!!
  ~~~~!!!!!!.                 .:!!!!!!!!!UWWW$$$ 
      :$$NWX!!:           .:!!!!!!XUWW$$$$$$$$$P 
      $$$$$##WX!:      .<!!!!UW$$$$"  $$$$$$$$# 
      $$$$$  $$$UX   :!!UW$$$$$$$$$   4$$$$$* 
      ^$$$B  $$$$\     $$$$$$$$$$$$   d$$R" 
        "*$bd$$$$      '*$$$$$$$$$$$o+#" 
             """"          """"""" 
NAME? psaunero
 ___________________________________ 
/ Hmm... psaunero?                  \
|                                   |
| That name sounds familiar... what |
\ message do you have for me?       /
 ----------------------------------- 
    \
     \
                                   .::!!!!!!!:.
  .!!!!!:.                        .:!!!!!!!!!!!!
  ~~~~!!!!!!.                 .:!!!!!!!!!UWWW$$$ 
      :$$NWX!!:           .:!!!!!!XUWW$$$$$$$$$P 
      $$$$$##WX!:      .<!!!!UW$$$$"  $$$$$$$$# 
      $$$$$  $$$UX   :!!UW$$$$$$$$$   4$$$$$* 
      ^$$$B  $$$$\     $$$$$$$$$$$$   d$$R" 
        "*$bd$$$$      '*$$$$$$$$$$$o+#" 
             """"          """"""" 
MESSAGE? Y2ZuaGFyZWI9MTQ4NjA4MzcwMg==
 ________________________________________ 
/ Ah yes... psaunero!                    \
|                                        |
| You're smarter than I thought. I can   |
| see why the instructor likes you.      |
|                                        |
| You met BOBBIT about 35 minutes ago... |
\ What took you so long?                 /
 ---------------------------------------- 
    \
     \
                                   .::!!!!!!!:.
  .!!!!!:.                        .:!!!!!!!!!!!!
  ~~~~!!!!!!.                 .:!!!!!!!!!UWWW$$$ 
      :$$NWX!!:           .:!!!!!!XUWW$$$$$$$$$P 
      $$$$$##WX!:      .<!!!!UW$$$$"  $$$$$$$$# 
      $$$$$  $$$UX   :!!UW$$$$$$$$$   4$$$$$* 
      ^$$$B  $$$$\     $$$$$$$$$$$$   d$$R" 
        "*$bd$$$$      '*$$$$$$$$$$$o+#" 
             """"          """"""" 
REASON? I had to go eat
 ______________________________________ 
/ Hmm... Sorry, kid. You got the gift, \
| but it looks like you're waiting for |
| something.                           |
|                                      |
| Your next life, maybe. Who knows?    |
\ That's the way these things go.      /
 -------------------------------------- 
    \
     \
                                   .::!!!!!!!:.
  .!!!!!:.                        .:!!!!!!!!!!!!
  ~~~~!!!!!!.                 .:!!!!!!!!!UWWW$$$ 
      :$$NWX!!:           .:!!!!!!XUWW$$$$$$$$$P 
      $$$$$##WX!:      .<!!!!UW$$$$"  $$$$$$$$# 
      $$$$$  $$$UX   :!!UW$$$$$$$$$   4$$$$$* 
      ^$$$B  $$$$\     $$$$$$$$$$$$   d$$R" 
        "*$bd$$$$      '*$$$$$$$$$$$o+#" 
             """"          """"""" 

Congratulations psaunero! You have reached the end of this journey.

I hope you learned something from the ORACLE :]

          ,       S   ; t Z     1   h I     E N   2 .     I D     ` ) 9 U   p D
          <           P # '     M 9   '     ^ '   v )   - ] u     L # U g   H S
          O           t K u     f U   U     u W     |   B "       A 2 i 5   W _
          ;           S : @     g N   ]     } a p   o   H e       s [ / 4   U 9
                      & ^       X =   >   w 2 v t   W   D 3       r V {     H u
            1         8     r   Q |   =   0 a < o   g Y W e       [ t x     a 9
    I     ' :         o     B     y   6   z P x _   c ' Y Q       a s w   } j w
    \     d ]         z     @     4   W   % + x P   D c S _       H w b   p ; t
    J     @ | _ ]     X     p     .   2     ? \ 0   . N y e       ` \ - N > $ w
    ]     2 % } e     *     T     m   :       Q Z     K F c       " ?   5 X k p
          N f * 1     2     :     e   b       . M     i H O       F H   m k _ I
* 6       I H _ \     3     y     T   1                 p z         {   = 8 6 1
| 6       8 \ h i     C i   K     1   i                 k Y         T   < o f *
u `       \ H [       n t   %     1                     x A         :   ) 0 D  
) |   {   [ = F         V   q     W                     8 g     <   >   [ D ?  
p     s   + k |   3 d   2   W W   8                     + O   w 2   g   f X    
Z     #   Y b L   g l     P C 6   !     (               }     N <       0 !    
y     r   & > W   A r     O ( @   a     6       V       X   Z } j       + e    
|     8   9 6 X   } !     ^ j c   g     @       <       k   @ x 7       M g    
r %   7 H   S z   " N     &   _   y     U       ]   m   K   u 5 h       ^ G    
A +     B   Y 3   6 V     =       j     t       h e \   C   o D q       m      
M \     x     b   ! '     I       d j   @       p E {   b   + ` E       _      
h >     e     ^   i A \   C         ,   V     g ` W W   v   t Q A       ,      
U Q 3   .     D   . ; "             k   }     Y e i )       K B }       T      


________________________________________________________________________________


WHAT I LEARNED:

This was definitely a fun activity. I think the most important thing I learned was to lear how to properly search the web
for help either using commands or acting in certain situations. Another important aspect of this Activity was how it forced me 
to really think and understand what was happening behind the scenes instead of just writing commands.For example, I understood
and had to think about what was happening when I connected to the port or when I created the script to unlock the lockbox.
