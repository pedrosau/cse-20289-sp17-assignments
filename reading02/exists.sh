#!/bin/sh

if [ $# = 0 ] ; then
   echo "Error: no arguments"
   exit 9
fi

for file in $*
do
if [ -e "$file" ] ; then
    echo "$file exists!"
 else
    echo "$file does not exist!"
    exit 5
 fi
  
done
