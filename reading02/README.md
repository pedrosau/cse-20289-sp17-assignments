Reading 02
==========

1) To run the script even if it's not executable I would use sh 
exists.sh instead of directly running the script.

2) To make the script executable you would need to change the 
permissions of the script.

3) To run the script directly I would write ./exists.sh

4) That line is used to let the kernel know that the file is a script and it is going 
to be executed by /bin/sh

5) It's going to display "$FILENAME exists !" with the name of the first 
file in the directory.

7) Tests if the file with name of the first parameter exists.

8) The script checks if a file exists.



