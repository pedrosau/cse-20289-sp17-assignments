Deductions:

Comments:
    We were specifically looking for the command in #2, but I'll assume you knew
    it.
    Also you usually don't want to exit with status 9, as that's the same exit
    code that a killed command gives, therefore it could cause confusion. 1 is
    usually the standard for script errors

Final Score: 4.0/4.0
Good job
